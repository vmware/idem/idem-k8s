import time
import uuid
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-k8s-service-" + str(int(time.time())),
}
RESOURCE_TYPE = "k8s.core.v1.service"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    metadata_name = PARAMETER["name"]
    metadata = {
        "name": metadata_name,
        "labels": {"name": "service-1"},
        "namespace": "default",
    }
    spec = {
        "type": "NodePort",
        "selector": {"app": "nginx"},
        "ports": [{"nodePort": 30163, "port": 8080, "targetPort": 80}],
    }
    PARAMETER["spec"] = spec
    PARAMETER["metadata"] = metadata

    # create k8s.core.v1.service
    response = await hub.states.k8s.core.v1.service.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.k8s.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert_service(
        spec,
        metadata,
        metadata_name,
        resource,
        "service-1",
        PARAMETER.get("resource_id")
        if PARAMETER.get("resource_id")
        else "resource_id_known_after_present",
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-name-invalid", depends=["present"])
async def test_exec_get_invalid_service(hub, ctx):
    service_name = str(int(time.time()))
    ret = await hub.exec.k8s.core.v1.service.get(
        ctx,
        name=service_name,
        resource_id=str(uuid.uuid4()),
    )
    assert ret["result"]
    assert hub.tool.k8s.comment_utils.get_empty_comment(
        resource_type=RESOURCE_TYPE, name=service_name
    )[0] in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-name", depends=["exec-get-by-name-invalid"])
async def test_exec_get_service(hub, ctx):
    ret = await hub.exec.k8s.core.v1.service.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        namespace="default",
    )
    assert ret["result"], ret["comment"]
    assert_service(
        PARAMETER["spec"],
        PARAMETER["metadata"],
        PARAMETER["name"],
        ret["ret"],
        label_value=PARAMETER["metadata"].get("labels").get("name"),
        resource_id=PARAMETER["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["exec-get-by-name"])
async def test_update(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    updated_label_name = "updated-service-name-" + str(int(time.time()))
    metadata = {
        "name": PARAMETER["name"],
        "labels": {"name": updated_label_name},
        "namespace": "default",
    }
    PARAMETER["metadata"] = metadata
    response = await hub.states.k8s.core.v1.service.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.k8s.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert_service(
        PARAMETER["spec"],
        PARAMETER["metadata"],
        PARAMETER["name"],
        resource,
        label_value=PARAMETER["metadata"].get("labels").get("name"),
        resource_id=PARAMETER["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.k8s.core.v1.service.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert "k8s.core.v1.service.present" in describe_response.get(
        PARAMETER["resource_id"]
    )
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("k8s.core.v1.service.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "k8s.core.v1.service.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert_service(
        PARAMETER["spec"],
        PARAMETER["metadata"],
        PARAMETER["name"],
        resource,
        label_value=PARAMETER["metadata"].get("labels").get("name"),
        resource_id=PARAMETER["resource_id"],
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete k8s.core.v1.service.
    ret = await hub.states.k8s.core.v1.service.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        metadata=PARAMETER["metadata"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert_service(
        PARAMETER["spec"],
        PARAMETER["metadata"],
        PARAMETER["name"],
        resource,
        label_value=PARAMETER["metadata"].get("labels").get("name"),
        resource_id=PARAMETER["resource_id"],
    )
    if __test:
        assert (
            hub.tool.k8s.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.k8s.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.k8s.core.v1.service.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        metadata=PARAMETER["metadata"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.k8s.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="none-resource", depends=["already-absent"])
async def test_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.k8s.core.v1.service.absent(
        ctx, name=PARAMETER["name"], metadata=PARAMETER["metadata"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.k8s.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.k8s.core.v1.service.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            metadata=PARAMETER["metadata"],
        )
        assert ret["result"], ret["comment"]


def assert_service(
    spec,
    metadata,
    metadata_name,
    resource,
    label_value="service-1",
    resource_id=None,
):
    assert spec.get("type") == resource.get("spec").get("type")
    assert metadata.get("name") == resource.get("metadata").get("name")
    if not resource_id:
        assert metadata_name == resource.get("resource_id")
    else:
        assert resource_id == resource.get("resource_id")
    assert label_value == resource.get("metadata").get("labels").get("name")
