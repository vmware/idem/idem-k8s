nginx-depl:
  k8s.apps.v1.deployment.present:
    - metadata:
        name: nginx-depl
        namespace: default
    - spec:
        replicas: 1
        selector:
          match_labels:
            app: nginx
        strategy:
          rolling_update:
            max_surge: 25%
            max_unavailable: 25%
          type: RollingUpdate
        template:
          metadata:
            labels:
              app: nginx
          spec:
            containers:
            - image: nginx:1.14.2
              image_pull_policy: IfNotPresent
              name: nginx
              ports:
              - container_port: 80
                protocol: TCP
              termination_message_path: /dev/termination-log
              termination_message_policy: File
            restart_policy: Always
